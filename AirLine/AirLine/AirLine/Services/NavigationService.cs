﻿
using AirLine.Pages;
using System.Threading.Tasks;

namespace AirLine.Services
{
    public class NavigationService
    {


        #region Methods
        public void SetMainPage(string pageName)
        {
            switch (pageName)
            {

                case "MasterPage":
                    App.Current.MainPage = new MasterPage();
                    break;
                case "RegisterPage":
                    App.Current.MainPage = new RegisterPage();
                    break;
                case "TerminosPage":
                    App.Current.MainPage = new TerminosPage();
                    break;
                case "ForgotPasswordPage":
                    App.Current.MainPage = new ForgotPasswordPage();
                    break; 
                case "LoginFacebookPage":
                    App.Current.MainPage = new LoginFacebookPage();
                    break;
                case "LoginPage":
                    App.Current.MainPage = new LoginPage();
                    break;
                    

            }
        }

        public async Task BackOnMaster()
        {
            await App.Navigator.PopAsync();
        }

        public async Task Navigate(string pageName)
        {

            App.Master.IsPresented = false;
            switch (pageName)
            {
                case "TerminosPage":
                    await App.Navigator.PushAsync(new TerminosPage());
                    break;

                case "HelpPage":
                    await App.Navigator.PushAsync(new HelpPage());
                    break;
                case "MyReservationPage":
                    await App.Navigator.PushAsync(new MyReservationPage());
                    break;
                    
                case "MasterPage":
                    await App.Navigator.PushAsync(new MasterPage());
                    break;
                case "DetailsPage":
                    await App.Navigator.PushAsync(new DetailsPage());
                    break;
                case "LoginPage":
                    await App.Navigator.PushAsync(new LoginPage());
                    break;
                case "ProfilePage":
                    await App.Navigator.PushAsync(new ProfilePage());
                    break;
                case "ChangePasswordPage":
                    await App.Navigator.PushAsync(new ChangePasswordPage());
                    break;
                    




                default:
                    break;
            }
        }

        public async Task Back()
        {
            await App.Navigator.PopAsync();
        }
        #endregion
    }
}
