﻿

using AirLine.Helpers;
using AirLine.Models;
using AirLine.Services;
using GalaSoft.MvvmLight.Command;
using System;
using System.ComponentModel;
using System.Windows.Input;

namespace AirLine.ViewModels
{
    public class MenuItemViewModel :INotifyPropertyChanged
    {
        #region Properties
        public string Title { get; set; }
        public string Icon { get; set; }
        public string PageName { get; set; }
        #endregion

        #region Attritubes
        private NavigationService navigationService;
        private DataService dataService;
        DialogService dialogService;
        User user;
        #endregion

        #region Constructor
        public MenuItemViewModel()
        {
            dialogService = new DialogService();
            navigationService = new NavigationService();
            dataService = new DataService();
        }
        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged; 
        #endregion

        #region Commands
        public ICommand NavigateCommand { get { return new RelayCommand(Navigate); } }


        #endregion

        #region Methods
        private async void Navigate()
        {
            var mainViewModel = MainViewModel.GetInstance();
            if (PageName == "LoginPage")
            {
                
             

                mainViewModel.CurrentUser.IsRemembered = false;
                dataService.Update(mainViewModel.CurrentUser);
                navigationService.SetMainPage("LoginPage");
            }
            else
            { 
            switch (PageName)
                {                
                    case "MyReservationPage":
                        await navigationService.Navigate(PageName);
                        break;
                    case "ProfilePage":
                                 
                                mainViewModel.Profile = new ProfileViewModel(mainViewModel.CurrentUser);                        
                               await navigationService.Navigate(PageName);
                                 
                          
                        break;
                        
                    default:
                        break;
                }
            }
        }
        #endregion

    }
}
