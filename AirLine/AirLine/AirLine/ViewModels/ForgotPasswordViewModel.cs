﻿
using AirLine.Helpers;
using AirLine.Models;
using AirLine.Services;
using GalaSoft.MvvmLight.Command;
using Plugin.Connectivity;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace AirLine.ViewModels
{
    public class ForgotPasswordViewModel : INotifyPropertyChanged
    {
        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Attributes
        private ApiService apiService;
        private DialogService dialogService;
        private NavigationService navigationService;
        private DataService dataService;
        private bool isRunning;
        private bool isEnabled;
        private string email;
        #endregion

        #region Properties
        public string Email
        {
            set
            {
                if (email != value)
                {
                    email = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Email"));
                }
            }
            get
            {
                return email;
            }
        }

        public bool IsRunning
        {
            set
            {
                if (isRunning != value)
                {
                    isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRunning"));
                }
            }
            get
            {
                return isRunning;
            }
        }

        public bool IsEnabled
        {
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsEnabled"));
                }
            }
            get
            {
                return isEnabled;
            }
        }
        #endregion
        #region Constructor
        public ForgotPasswordViewModel()
        {
            apiService = new ApiService();
            dialogService = new DialogService();
            navigationService = new NavigationService();
            dataService = new DataService();
            IsEnabled = true;
        }
        #endregion

        #region Commands
        public ICommand SendNewPasswordCommand { get { return new RelayCommand(SendNewPassword); } }



        public ICommand CancelCommand { get { return new RelayCommand(Cancel); } }


        #endregion
        #region Methods
        private async void SendNewPassword()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ConfigValidation);
                return;
            }

            var isReachable = await CrossConnectivity.Current.IsRemoteReachable("google.com");
            if (!isReachable)
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.InternetValidation);
                return;
            }

            if (string.IsNullOrEmpty(Email))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationEmail);
                return;
            }

            IsRunning = true;
            IsEnabled = false;

            var parameters = dataService.First<Parameter>(false);
            var response = await apiService.PasswordRecovery(
                parameters.URLBase, "/api", "/Users/PasswordRecovery", Email);

            IsRunning = false;
            IsEnabled = true;

            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.RecoveryPasswordError);
                return;
            }

            await dialogService.ShowMessage(Lenguages.Confirmation, Lenguages.RecoveryPasswordSuccess);
            navigationService.SetMainPage("LoginPage");
        }
        private void Cancel()
        {
            navigationService.SetMainPage("LoginPage");
        }
        #endregion
    }
}
