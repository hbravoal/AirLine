﻿using AirLine.Helpers;
using AirLine.Models;
using AirLine.Services;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AirLine.ViewModels
{
    public class MyReservationViewModel: Reservation,INotifyPropertyChanged
    {
        #region Singleton
        private static MyReservationViewModel instance;

        public static MyReservationViewModel GetInstance()
        {

            if (instance == null)
            {
                instance = new MyReservationViewModel();
            }

            return instance;
        }
        #endregion
        #region Attributes


        private bool isRunning;
        private bool isRefreshing;
        List<Fly> flys;
        List<Reservation> reservation_;
        ObservableCollection<Reservation> reservation;
        #endregion
        #region Properties
        public ObservableCollection<Reservation> Reservation
        {
            get
            {
                return reservation;
            }
            set
            {
                if (reservation != value)
                {
                    reservation = value;
                    PropertyChanged?.Invoke(
                        this, new PropertyChangedEventArgs(nameof(Reservation)));
                }
            }
        }
        public bool IsRefreshing
        {
            set
            {
                if (isRefreshing != value)
                {
                    isRefreshing = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRefreshing"));
                }
            }
            get
            {
                return isRefreshing;
            }
        }
        public bool IsRunning
        {
            set
            {
                if (isRunning != value)
                {
                    isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRunning"));
                }
            }
            get
            {
                return isRunning;
            }
        } 
        #endregion
        public MyReservationViewModel()
        {

            instance = this;
            flys = new List<Fly>();
            reservation_ = new List<Models.Reservation>();
            
            Reservation = new ObservableCollection<Reservation>();
            apiService = new ApiService();
            dialogService = new DialogService();
            navigationService = new NavigationService();
            dataService = new DataService();
            
            LoadReservation();
        }
        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
        #region Methods

        public void Add(Reservation fly)
        {
            IsRefreshing = true;
            Reservation.Add(fly);
            Reservation = new ObservableCollection<Reservation>(
                Reservation.OrderBy(c => c.Price));
            IsRefreshing = false;
        }

        async void LoadReservation()
        {
            var parameters = dataService.First<Parameter>(false);


            var response = await apiService.Get<Reservation>(parameters.URLBase, "/api/", "Reservations");
            if (!response.IsSuccess)
            {

                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ProblemOcurred);
                return;
            }
            var reser = (List<Reservation>)response.Result;
            
            Reservation.Clear();

            Reservation = new ObservableCollection<Reservation>(reser);

        }

        public ICommand SearchProductCommand { get { return new RelayCommand(SearchProduct); } }
        async void SearchProduct()
        {
            var parameters = dataService.First<Parameter>(false);            
            var response = await apiService.Get<Reservation>(parameters.URLBase, "/Users/", "GetReservationWithTI");
            if (!response.IsSuccess)
            {
                IsRunning = false;
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ProblemOcurred);
                return;
            }
            

            
            
            var reser = (List<Reservation>)response.Result;
            dataService.Save<Reservation>(reser);
            Reservation.Clear();
            Reservation = new ObservableCollection<Reservation>(reser);
            IsRefreshing = false;
        }
        #endregion
        #region Services
        private ApiService apiService;
        private DialogService dialogService;
        NavigationService navigationService;
        DataService dataService;
        #endregion
    }
}
