﻿using AirLine.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AirLine.Models;
using Xamarin.Forms;
using Plugin.Media.Abstractions;
using AirLine.Services;
using System.ComponentModel;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Plugin.Connectivity;
using AirLine.Helpers;
using Plugin.Media;

namespace AirLine.ViewModels
{
    public class ProfileViewModel : User, INotifyPropertyChanged
    {
        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
        #region Attributes
        private ApiService apiService;
        private DialogService dialogService;
        private NavigationService navigationService;
        private DataService dataService;
        private bool isRunning;
        private bool isEnabled;
        private bool allowToModify;
        
        
        private ImageSource imageSource;
        private MediaFile file;
        private User currentUser;
        #endregion

        #region Properties
        public bool IsRunning
        {
            set
            {
                if (isRunning != value)
                {
                    isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRunning"));
                }
            }
            get
            {
                return isRunning;
            }
        }

        public bool IsEnabled
        {
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsEnabled"));
                }
            }
            get
            {
                return isEnabled;
            }
        }

        public bool AllowToModify
        {
            set
            {
                if (allowToModify != value)
                {
                    allowToModify = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AllowToModify"));
                }
            }
            get
            {
                return allowToModify;
            }
        }

        public ImageSource ImageSource
        {
            set
            {
                if (imageSource != value)
                {
                    imageSource = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ImageSource"));
                }
            }
            get
            {
                return imageSource;
            }
        }

       
        #endregion


       
        public ProfileViewModel(User currentUser)
        {
            
            this.currentUser = currentUser;

            apiService = new ApiService();
            dialogService = new DialogService();
            navigationService = new NavigationService();
            dataService = new DataService();

           
            FirstName = currentUser.FirstName;
            LastName = currentUser.LastName;
            Phone = currentUser.Phone;           
            Address = currentUser.Address;
            Picture = currentUser.Picture;
            AllowToModify = currentUser.UserTypeId == 1;
            Email = currentUser.Email;
            IsEnabled = true;
            AllowToModify = currentUser.UserTypeId == 1;

            
        }

        #region Commands
        public ICommand ChangePasswordCommand { get { return new RelayCommand(ChangePassword); } }
        public ICommand TakePictureCommand { get { return new RelayCommand(TakePicture); } }
        public ICommand SaveCommand { get { return new RelayCommand(Save); } }
        #endregion
        #region Methods
        private async void TakePicture()
        {
            if (!AllowToModify)
            {
                return;
            }
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await dialogService.ShowMessage("No Camera", ":( No camera available.");
                return;
            }

            IsRunning = true;

            file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                Directory = "Sample",
                Name = "test.jpg",
                PhotoSize = PhotoSize.Small,
            });

            if (file != null)
            {
                ImageSource = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    return stream;
                });
            }

            IsRunning = false;
        }
        private async void ChangePassword()
        {
            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.ChangePassword = new ChangePasswordViewModel();
            await navigationService.Navigate("ChangePasswordPage");
        }
        private async void Save()
        {
            if (string.IsNullOrEmpty(FirstName))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationSubName);
                return;
            }
            if (string.IsNullOrEmpty(LastName))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationSubName);
                return;
            }
          

            if (string.IsNullOrEmpty(Email))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationEmail);
                return;
            }
          

            if (string.IsNullOrEmpty(Phone))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationPhone);
                return;
            }
            if (string.IsNullOrEmpty(Address))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationPhone);
                return;
            }
            if (!CrossConnectivity.Current.IsConnected)
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ConfigValidation);
                return;
            }
            var isReachable = await CrossConnectivity.Current.IsRemoteReachable("google.com");
            if (!isReachable)
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.InternetValidation);
                return;
            }
            return;
            

            IsRunning = true;
            IsEnabled = false;

            byte[] imageArray = null;
            if (file != null)
            {
                imageArray = FilesHelper.ReadFully(file.GetStream());
                file.Dispose();
            }

            var user = new User
            {
                Email = Email,
                Password = Password,
                Address = Address,
                FirstName = FirstName,
                LastName = LastName,
                Phone = Phone,
                UserTypeId = 1,
                imageArray = imageArray,
               
            };

            var parameters = dataService.First<Parameter>(false);
            var response = await apiService.Put(parameters.URLBase, "/api", "/Users",
                currentUser.TokenType, currentUser.AccessToken, user);

            if (!response.IsSuccess)
            {
                IsRunning = false;
                IsEnabled = true;
                await dialogService.ShowMessage("Error", response.Message);
                return;
            }

            response = await apiService.GetUserByEmail(
                parameters.URLBase,
                "/api",
                "/Users/GetUserByEmail",
                currentUser.TokenType,
                currentUser.AccessToken,
                Email);

            var newUser = (User)response.Result;
            newUser.AccessToken = currentUser.AccessToken;
            newUser.TokenType = currentUser.TokenType;
            newUser.TokenExpires = currentUser.TokenExpires;
            newUser.IsRemembered = currentUser.IsRemembered;
            newUser.Password = currentUser.Password;            
            dataService.DeleteAllAndInsert(newUser.UserType);
            dataService.DeleteAllAndInsert(newUser);

            IsRunning = false;
            IsEnabled = true;

            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.CurrentUser = newUser;
            await navigationService.Back();
        }
        #endregion
    }

}
