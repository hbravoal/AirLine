﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AirLine.Models;
using System.ComponentModel;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using AirLine.Services;
using AirLine.Helpers;

namespace AirLine.ViewModels
{
    public class DetailsViewModel : INotifyPropertyChanged
    {
        #region Attritubes
        private string letter;
        private string capacity;
        private double price;
        private string airLineModelName;
        private DateTime data;
        private ApiService apiService;
        private Fly fly;
        private NavigationService navigationService;
        private DataService dataService;
        DialogService dialogService;
        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
        #region Properties
        public Fly Fly
        {
            set
            {
                if (fly != value)
                {
                    fly = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Fly"));
                }
            }
            get
            {
                return fly;
            }
        }
        public string Letter
        {
            set
            {
                if (letter != value)
                {
                    letter = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Letter"));
                }
            }
            get
            {
                return letter;
            }
        }
        public string Capacity
        {
            set
            {
                if (capacity != value)
                {
                    capacity = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Capacity"));
                }
            }
            get
            {
                return capacity;
            }
        }
        public double Price
        {
            set
            {
                if (price != value)
                {
                    price = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Price"));
                }
            }
            get
            {
                return price;
            }
        }
        public string Name
        {
            set
            {
                if (airLineModelName != value)
                {
                    airLineModelName = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
                }
            }
            get
            {
                return airLineModelName;
            }
        }
        public DateTime Data
        {
            set
            {
                if (data != value)
                {
                    data = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Data"));
                }
            }
            get
            {
                return data;
            }
        }
        #endregion


        public DetailsViewModel(Fly to)
        {
            apiService = new ApiService();
            dialogService = new DialogService();
            dataService = new DataService();
            navigationService = new NavigationService();

            this.fly = to;
            Name = to.Airport.Name;
            Letter = to.Bed.Letter;
            Price = to.Reservation.Price;
            Capacity = to.AirPlane.Capacity;
            Data = fly.Reservation.Date;

        }

        #region Events
        public ICommand ReservationCommand { get { return new RelayCommand(ReservationC); } }
        #endregion

        async void ReservationC()
        {

            var user = dataService.First<User>(false);
            if (user != null && user.IsRemembered && user.TokenExpires > DateTime.Now)
            {

                var parameters = dataService.First<Parameter>(false);
                var model = new Reservation
                {
                    Date = this.Data,
                    Observations = "Agregado",
                    Price = this.Price,


                };
                var response = await apiService.Post<Reservation>(parameters.URLBase, "/api/", "Reservations", model);
                if (!response.IsSuccess)
                {

                    await dialogService.ShowMessage(Lenguages.Error, Lenguages.ProblemOcurred);
                    return;
                }
                await dialogService.ShowMessage(Lenguages.Confirmation, Lenguages.Confirmation);
                var p = MyReservationViewModel.GetInstance();

                await dialogService.ShowMessage(Lenguages.Error, Lenguages.MessageLogiIn);
                navigationService.SetMainPage("LoginPage");
                p.Add(model);



            }
        }

    }
}