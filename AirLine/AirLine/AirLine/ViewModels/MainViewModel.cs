﻿using AirLine.Helpers;
using AirLine.Models;
using AirLine.Services;
using GalaSoft.MvvmLight.Command;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;

namespace AirLine.ViewModels
{ 
  public  class MainViewModel : INotifyPropertyChanged
    {
        #region Attributes
        public User currentUser;
        NavigationService navigationService;
        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Singleton
        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {

            if (instance == null)
            {
                instance = new MainViewModel();
            }

            return instance;
        }
        #endregion

        #region Properties       
        public ObservableCollection<MenuItemViewModel> Menu { get; set; }
        public HomeViewModel Home { get; set; }
        public ProfileViewModel Profile { get; set; }
        public LoginViewModel Login{ get; set; }
        public DetailsViewModel Details { get; set; }
        public TerminosViewModel Terminos { get; set; }
        public RegisterViewModel Register { get; set; }
        public ChangePasswordViewModel ChangePassword { get; set; }
        public ForgotPasswordViewModel ForgotPassword { get; set; }
        public MyReservationViewModel MyReservations { get; set; }




        public User CurrentUser
        {
            set
            {
                if (currentUser != value)
                {
                    currentUser = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CurrentUser"));
                }
            }
            get
            {
                return currentUser;
            }
        }
        #endregion

        #region Constructor
        public MainViewModel()
        {
            instance = this;
            navigationService = new NavigationService();
            Menu = new ObservableCollection<MenuItemViewModel>();
            Home = new HomeViewModel();
            Login = new LoginViewModel();
            MyReservations = new MyReservationViewModel();
            Register = new RegisterViewModel();
            Terminos = new TerminosViewModel();
            ForgotPassword = new ForgotPasswordViewModel();
            ChangePassword = new ChangePasswordViewModel();
            
            LoadMenu();
        }
        #endregion
        #region Commands
        public ICommand HelpCommand { get { return new RelayCommand(HelpC); } } 
        #endregion
        #region Methods
        private void LoadMenu()
        {
            Menu = new ObservableCollection<MenuItemViewModel>();
            Menu.Add(new MenuItemViewModel
            {
                Title = Lenguages.MyReservation,
                Icon = "ic_airplane_up.png",
                PageName = "MyReservationPage"

            });
          
            Menu.Add(new MenuItemViewModel
            {
                Title = Lenguages.Profile,
                Icon = "ic_profile.png",
                PageName = "ProfilePage"

            });
            Menu.Add(new MenuItemViewModel
            {
                Title = Lenguages.Logout,
                Icon = "ic_smile.png",
                PageName = "LoginPage"
            });
        }
        public void SetCurrentUser(User user)
        {
            CurrentUser = user;
        }
        



        async void HelpC()
        {

            await navigationService.Navigate("TerminosPage");
        }
        #endregion
    }
}
