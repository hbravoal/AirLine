﻿
using AirLine.Helpers;
using AirLine.Models;
using AirLine.Services;
using GalaSoft.MvvmLight.Command;
using Plugin.Connectivity;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System;

namespace AirLine.ViewModels
{
    public class HomeViewModel : Fly,INotifyPropertyChanged
    {
        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Services
        private ApiService apiService;
        private DialogService dialogService;

        #endregion

        #region Attributes

        List<Fly> flys;
        List<User> users;
        ObservableCollection<Fly> _flys;
        NavigationService navigationService;
        DataService dataService;
        private bool isRunning;
        private bool isRefreshing;
        public User user;
        public string Filter { get; set; }
        #endregion

        #region Sigleton
        static HomeViewModel instance;

        public static HomeViewModel GetInstance()
        {
            if (instance == null)
            {
                return new HomeViewModel();
            }

            return instance;
        }
        #endregion

        #region Properties
        public ObservableCollection<Fly> Flys
        {
            get
            {
                return _flys;
            }
            set
            {
                if (_flys != value)
                {
                    _flys = value;
                    PropertyChanged?.Invoke(
                        this, new PropertyChangedEventArgs(nameof(Flys)));
                }
            }
        }
        public bool IsRefreshing
        {
            set
            {
                if (isRefreshing != value)
                {
                    isRefreshing = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRefreshing"));
                }
            }
            get
            {
                return isRefreshing;
            }
        }
        public bool IsRunning
        {
            set
            {
                if (isRunning != value)
                {
                    isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRunning"));
                }
            }
            get
            {
                return isRunning;
            }
        }
       
        #endregion

        #region Constructor
        public HomeViewModel()
        {
            instance = this;
            flys = new List<Fly>();
            Flys = new ObservableCollection<Fly>();
            apiService = new ApiService();
            dialogService = new DialogService();
            navigationService = new NavigationService();
            dataService = new DataService();
            LoadLocalData();
        }
        #endregion
        #region Methods
        public void Update(Fly fly)
        {


            IsRefreshing = true;
            LoadLocalData();
            IsRefreshing = false;

        }
        public void Add(Fly fly)
        {
            IsRefreshing = true;
            Flys.Add(fly);
            Flys = new ObservableCollection<Fly>(
                Flys.OrderBy(c => c.Airport));
            IsRefreshing = false;
        }
        public async Task Delete(Fly fly)
        {
            IsRefreshing = true;

            var connection = await apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                IsRefreshing = false;
                await dialogService.ShowMessage(Lenguages.Error, connection.Message);
                return;
            }

            var mainViewModel = MainViewModel.GetInstance();
            var parameters = dataService.First<Parameter>(false);
            var user = dataService.First<User>(true);
            var response = await apiService.Delete(
                parameters.URLBase,
                "/api",
                "/fly",
                user.TokenType,
                user.AccessToken,
                fly);

            if (!response.IsSuccess)
            {
                IsRefreshing = false;
                await dialogService.ShowMessage(
                    Lenguages.Error,
                    response.Message);
                return;
            }

            Flys.Remove(fly);
            Flys = new ObservableCollection<Fly>(
                Flys.OrderBy(c => c.Airport));

            IsRefreshing = false;
        }

        public async void LoadLocalData()
        {

            IsRunning = true;
            if (!CrossConnectivity.Current.IsConnected)
            {
                IsRunning = false;

                await dialogService.ShowMessage(Lenguages.Error, Lenguages.InternetValidation);

            }

            var isReachable = await CrossConnectivity.Current.IsRemoteReachable("google.com");
            if (!isReachable)
            {
                IsRunning = false;

                
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ConfigValidation);
                return;
            }
            var parameters = dataService.First<Parameter>(false);
            var user = dataService.First<User>(true);
            var response = await apiService.Get<Fly>(parameters.URLBase, "/api/","Flies");
            
            if (!response.IsSuccess)
            {
                IsRunning = false;
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ProblemOcurred);
                return;
            }
            flys = (List<Fly>)response.Result;
            
            Flys.Clear();
            Flys = new ObservableCollection<Fly>(flys);
            dataService.Save<Fly>(flys);

            
            IsRefreshing = false;
            


        }


        public void LoadLocalDataOutConnection()
        {
            var fly = dataService.Get<Fly>(true);

            Flys = new ObservableCollection<Fly>(fly);
            

        }

    


        #endregion

        #region Command
        public ICommand RefreshCommand
        {
            get { return new RelayCommand(Refresh); }
        }
        

        private void Refresh()
        {
            LoadLocalData();
        }
       
       


        #endregion
    }
}
