﻿using AirLine.Helpers;
using AirLine.Models;
using AirLine.Services;
using GalaSoft.MvvmLight.Command;
using Plugin.Connectivity;
using System.ComponentModel;
using System.Windows.Input;

namespace AirLine.ViewModels
{
  public  class LoginViewModel :INotifyPropertyChanged
    {
        #region Attributes
        private ApiService apiService;
        private DialogService dialogService;
        private NavigationService navigationService;
        private DataService dataService;
        private string email;
        private string password;
        private bool isRunning;
        private bool isEnabled;
        private bool isRemembered;
        

        #endregion

        #region Properties
        public string Email
        {
            set
            {
                if (email != value)
                {
                    email = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Email"));
                }
            }
            get
            {
                return email;
            }
        }

        public string Password
        {
            set
            {
                if (password != value)
                {
                    password = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Password"));
                }
            }
            get
            {
                return password;
            }
        }

        public bool IsRunning
        {
            set
            {
                if (isRunning != value)
                {
                    isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRunning"));
                }
            }
            get
            {
                return isRunning;
            }
        }

        public bool IsEnabled
        {
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsEnabled"));
                }
            }
            get
            {
                return isEnabled;
            }
        }

        public bool IsRemembered
        {
            set
            {
                if (isRemembered != value)
                {
                    isRemembered = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRemembered"));
                }
            }
            get
            {
                return isRemembered;
            }
        }

        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Constructors
        public LoginViewModel()
        {

            apiService = new ApiService();
            dialogService = new DialogService();
            navigationService = new NavigationService();
            dataService = new DataService();

            IsEnabled = true;
            IsRemembered = true;
        }
        #endregion

        #region Commands

        public ICommand LoginFacebookCommand { get { return new RelayCommand(LoginFacebook); } }
        public ICommand ForgotPasswordCommand { get { return new RelayCommand(ForgotPassword); } }
        public ICommand LoginCommand { get { return new RelayCommand(Login); } }
        public ICommand RegisterCommand { get { return new RelayCommand(Register); } }
        
        #endregion
        #region Methods
        private void ForgotPassword()
        {
            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.ForgotPassword = new ForgotPasswordViewModel();

            navigationService.SetMainPage("ForgotPasswordPage");
        }
        private void LoginFacebook()
        {
            navigationService.SetMainPage("LoginFacebookPage");
        }
       
        private void Register()
        {
            navigationService.SetMainPage("RegisterPage");
        }


        private async void Login()
        {

            if (string.IsNullOrEmpty(Email))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.EmailValidation);
                return;
            }

            if (string.IsNullOrEmpty(Password))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.PasswordValidation);
                return;
            }
            IsRunning = true;
            IsEnabled = false;

            if (!CrossConnectivity.Current.IsConnected)
            {
                IsRunning = false;
                IsEnabled = true;
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.InternetValidation);

            }

            var isReachable = await CrossConnectivity.Current.IsRemoteReachable("google.com");
            if (!isReachable)
            {
                IsRunning = false;
                IsEnabled = true;
                LoadLocalData();
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ConfigValidation);
                return;
            }
            var parameters = dataService.First<Parameter>(false);

            var token = await apiService.GetToken(parameters.URLBase, Email, Password);

            if (token == null)
            {
                IsRunning = false;
                IsEnabled = true;
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.TokenValidation);
                Password = null;
                return;
            }
            if (string.IsNullOrEmpty(token.AccessToken))
            {
                IsRunning = false;
                IsEnabled = true;
                await dialogService.ShowMessage(Lenguages.Error, token.ErrorDescription);
                Password = null;
                return;
            }


            var response = await apiService.GetUserByEmail(parameters.URLBase, "/api", "/Users/GetUserByEmail", token.TokenType, token.AccessToken, token.UserName);
            if (!response.IsSuccess)
            {
                IsRunning = false;
                IsEnabled = true;

                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ProblemOcurred);
                return;
            }

            IsRunning = false;
            IsEnabled = true;
            var user = (User)response.Result;

            user.Password = Password;

            user.AccessToken = token.AccessToken;
            user.TokenType = token.TokenType;
            user.TokenExpires = token.Expires;
            user.IsRemembered = IsRemembered;


            user.UserTypeId = 1;

            dataService.DeleteAllAndInsert<UserType>(user.UserType);
            dataService.DeleteAllAndInsert<User>(user);



            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.CurrentUser = user;

            navigationService.SetMainPage("MasterPage");

        }

        private void LoadLocalData()
        {

          var  user = dataService.First<User>(false);
            
            
        }
        


        #endregion
    }
}
