﻿using AirLine.Services;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AirLine.ViewModels
{
  public   class TerminosViewModel
    {
        #region Attributes
        public NavigationService navigationService;
        #endregion
        #region Constructor
        public TerminosViewModel()
        {
            navigationService = new NavigationService();
        }
        #endregion
        #region Commands
        public ICommand CancelCommand { get { return new RelayCommand(Cancel); } }
        #endregion
        #region Methods
        private void Cancel()
        {
            navigationService.SetMainPage("RegisterPage");

        }
        #endregion
    }
}
