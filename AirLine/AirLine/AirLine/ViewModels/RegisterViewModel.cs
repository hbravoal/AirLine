﻿

using AirLine.Helpers;
using AirLine.Models;
using AirLine.Services;
using GalaSoft.MvvmLight.Command;
using Plugin.Connectivity;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace AirLine.ViewModels
{
   public class RegisterViewModel : INotifyPropertyChanged
    {
        #region Attributes
        private ApiService apiService;
        private DialogService dialogService;
        private NavigationService navigationService;
        private DataService dataService;
        private string address;

        private string firstName;
        private string lastName;
        private string password;
        private string passwordConfirm;
        private string email;
        private string phone;
      
        private bool isRunning;
        private bool isEnabled;
        private MediaFile file;
        private ImageSource imageSource = "upload_photo2.png";
        #endregion

        #region Constructor
        public RegisterViewModel()
        {
            dialogService = new DialogService();
            apiService = new ApiService();
            navigationService = new NavigationService();
            dataService = new DataService();
        }

        #endregion

        #region Properties
        public User user { get; set; }
        public string Phone
        {
            set
            {
                if (phone != value)
                {
                    phone = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Phone"));
                }
            }
            get { return phone; }
        }
        public string FirstName
        {
            set
            {
                if (firstName != value)
                {
                    firstName = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FirstName"));
                }
            }
            get { return firstName; }
        }
        public string LastName
        {
            set
            {
                if (lastName != value)
                {
                    lastName = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("LastName"));
                }
            }
            get { return lastName; }
        }
        public string Password
        {
            set
            {
                if (password != value)
                {
                    password = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Password"));
                }
            }
            get { return password; }
        }
        public string PasswordConfirm
        {
            set
            {
                if (passwordConfirm != value)
                {
                    passwordConfirm = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("PasswordConfirm"));
                }
            }
            get { return password; }
        }
        public string Email
        {
            set
            {
                if (email != value)
                {
                    email = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Email"));
                }
            }
            get
            {
                return email;
            }
        }
        public string Address
        {
            set
            {
                if (address != value)
                {
                    address = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Address"));
                }
            }
            get
            {
                return address;
            }
        }

        public bool IsRunning
        {
            set
            {
                if (isRunning != value)
                {
                    isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRunning"));
                }
            }
            get
            {
                return isRunning;
            }
        }
        public bool IsEnabled
        {
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsEnabled"));
                }
            }
            get
            {
                return isEnabled;
            }
        }
        public ImageSource ImageSource
        {
            set
            {
                if (imageSource != value)
                {
                    imageSource = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ImageSource"));
                }
            }
            get
            {
                return imageSource;
            }
        }

        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Command
        public ICommand RegisterCommand { get { return new RelayCommand(Register); } }

        public ICommand CancelCommand { get { return new RelayCommand(Cancel); } }
        public ICommand TerminosCommand { get { return new RelayCommand(Terminos); } }
        public ICommand TakePictureCommand { get { return new RelayCommand(TakePicture); } }
        #endregion

        #region Methods
        private async void TakePicture()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await dialogService.ShowMessage("No Camera", ":( No camera available.");
                return;
            }

            IsRunning = true;

            file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                Directory = "Sample",
                Name = "test.jpg",
                PhotoSize = PhotoSize.Small,
            });

            if (file != null)
            {
                ImageSource = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    return stream;
                });
            }

            IsRunning = false;
        }
        private void Terminos()
        {
            navigationService.SetMainPage("TerminosPage");
        }
        private void Cancel()
        {
            navigationService.SetMainPage("LoginPage");
        }
        private async void Register()
        {


            if (string.IsNullOrEmpty(firstName))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationSubName);
                return;
            }
            if (string.IsNullOrEmpty(lastName))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationSubName);
                return;
            }
        
            if (string.IsNullOrEmpty(email))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationEmail);
                return;
            }
            if (string.IsNullOrEmpty(password))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationPassword);
                return;
            }
            if (string.IsNullOrEmpty(passwordConfirm))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationPassword2);
                return;
            }

            if (Password != PasswordConfirm)
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationPasswords);
                return;
            }


            if (string.IsNullOrEmpty(phone))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationPhone);
                return;
            }
           

            if (!CrossConnectivity.Current.IsConnected)
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ConfigValidation);
                return;
            }
            var isReachable = await CrossConnectivity.Current.IsRemoteReachable("google.com");
            if (!isReachable)
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.InternetValidation);
                return;
            }

            IsRunning = true;
            IsEnabled = false;
            var imageArray = FilesHelper.ReadFully(file.GetStream());
            file.Dispose();

            user = new User
            {
                Email = email,
                Password = password,
                Address = address,
                FirstName = firstName,
                LastName = lastName,
                Phone = phone,
                UserTypeId = 1,
                imageArray = imageArray,                
             
                
            };

            var parameters = dataService.First<Parameter>(false);
            var response = await apiService.Post(parameters.URLBase, "/api", "/Users", user);

            IsRunning = false;
            IsEnabled = true;

            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage(Lenguages.Error, response.Message);
                return;
            }



            dataService.Insert<User>(user);
            await dialogService.ShowMessage(Lenguages.Confirmation, Lenguages.MessageUserCreated);
            navigationService.SetMainPage("LoginPage");
        }
        #endregion
    }
}
