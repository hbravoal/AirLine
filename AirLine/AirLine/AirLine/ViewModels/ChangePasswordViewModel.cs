﻿using AirLine.Classes;
using AirLine.Helpers;
using AirLine.Models;
using AirLine.Services;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AirLine.ViewModels
{
   public class ChangePasswordViewModel:INotifyPropertyChanged
    {
        #region Attributes
        private ApiService apiService;
        private DialogService dialogService;
        private NavigationService navigationService;
        private DataService dataService;
        private bool isRunning;
        private bool isEnabled;

        #endregion
        #region Properties
        public bool IsRunning
        {
            set
            {
                if (isRunning != value)
                {
                    isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRunning"));
                }
            }
            get
            {
                return isRunning;
            }
        }

        public bool IsEnabled
        {
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsEnabled"));
                }
            }
            get
            {
                return isEnabled;
            }
        }

        public string CurrentPassword { get; set; }

        public string NewPassword { get; set; }

        public string ConfirmPassword { get; set; }
        #endregion



        #region Constructor
        public ChangePasswordViewModel(ProfileViewModel profileViewModel)
        {
            apiService = new ApiService();
            dialogService = new DialogService();
            navigationService = new NavigationService();
            dataService = new DataService();

            IsEnabled = true;
            IsEnabled = true;
            CurrentPassword = profileViewModel.Password;

        }

        public ChangePasswordViewModel()
        {
            apiService = new ApiService();
            dialogService = new DialogService();
            navigationService = new NavigationService();
            dataService = new DataService();
            IsEnabled = true;
            IsEnabled = true;
        }
        #endregion

        #region Interfaces
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
        #region Commands
        public ICommand ChangePasswordCommand { get { return new RelayCommand(ChangePassword); } }
        public ICommand CancelCommand { get { return new RelayCommand(Cancel); } }

        async void Cancel()
        {
            await navigationService.BackOnMaster();
        }

        #endregion
        #region Methods
        private async void ChangePassword()
        {
            if (string.IsNullOrEmpty(CurrentPassword))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationPassword);
                return;
            }

            var mainViewModel = MainViewModel.GetInstance();

            if (mainViewModel.CurrentUser.Password != CurrentPassword)
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationPassword2);
                return;
            }

            if (string.IsNullOrEmpty(NewPassword))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationPassword2);
                return;
            }

            if (NewPassword == CurrentPassword)
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationPasswords);
                return;
            }

           

            if (string.IsNullOrEmpty(ConfirmPassword))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.ValidationPassword2);
                return;
            }

            if (NewPassword != ConfirmPassword)
            {
                await dialogService.ShowMessage(Lenguages.Error,Lenguages.ValidationPasswords);
                return;
            }

            IsRunning = true;
            IsEnabled = false;

            var parameters = dataService.First<Parameter>(false);
            var user = dataService.First<User>(false);

            var request = new ChangePasswordRequest
            {
                CurrentPassword = CurrentPassword,
                Email = user.Email,
                NewPassword = NewPassword,
            };

            var response = await apiService.ChangePassword(parameters.URLBase, "/api", "/Users/ChangePassword",
                user.TokenType, user.AccessToken, request);

            IsRunning = false;
            IsEnabled = true;

            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage(Lenguages.Error, response.Message);
                return;
            }

            await dialogService.ShowMessage(Lenguages.Confirmation, Lenguages.MessagePasswordChanged);
            await navigationService.Back();
        }
        #endregion
    }
}
