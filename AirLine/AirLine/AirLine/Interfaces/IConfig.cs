﻿
using SQLite.Net.Interop;

namespace AirLine.Interfaces
{

    public interface IConfig
    {
        string DirectoryDB { get; }

        ISQLitePlatform Platform { get; }
    }
}
