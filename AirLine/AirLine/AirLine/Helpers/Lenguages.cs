﻿

namespace AirLine.Helpers
{
    using Xamarin.Forms;
    using Interfaces;
    
    using Resources;

    public static class Lenguages
    {
        static Lenguages()
        {
            var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
            Resource.Culture = ci;
            DependencyService.Get<ILocalize>().SetLocale(ci);
        }

        public static string Error
        {
            get { return Resource.Error; }
        }
        public static string Title
        {
            get { return Resource.Title; }
        }
        public static string Confirmation
        {
            get { return Resource.Confirmation; }
        }
        public static string RecoveryPasswordError
        {
            get { return Resource.RecoveryPasswordError; }
        }
        public static string RecoveryPasswordSuccess
        {
            get { return Resource.RecoveryPasswordSuccess; }
        }
        public static string MessageNoDataFirst
        {
            get { return Resource.MessageNoDataFirst; }
        }
    
        public static string EmailValidation
        {
            get { return Resource.EmailValidation; }
        }
        public static string PasswordValidation
        {
            get { return Resource.PasswordValidation; }
        }
        public static string InternetValidation
        {
            get { return Resource.InternetValidation; }
        }
        public static string ConfigValidation
        {
            get { return Resource.ConfigValidation; }
        }
        public static string TokenValidation
        {
            get { return Resource.TokenValidation; }
        }
       
        public static string ProblemOcurred
        {
            get { return Resource.ProblemOcurred; }
        }
        public static string MenuAsk
        {
            get { return Resource.MenuAsk; }
        }
        public static string MenuLogout
        {
            get { return Resource.MenuLogout; }
        }
        public static string MenuProfile
        {
            get { return Resource.MenuProfile; }
        }
        public static string ValidationAddress
        {
            get { return Resource.ValidationAddress; }
        }
        public static string ValidationName
        {
            get { return Resource.ValidationName; }
        }
        public static string ValidationPassword
        {
            get { return Resource.ValidationPassword; }
        }
        public static string ValidationPassword2
        {
            get { return Resource.ValidationPassword2; }
        }
        public static string ValidationPasswords
        {
            get { return Resource.ValidationPasswords; }
        }
        public static string ValidationPhone
        {
            get { return Resource.ValidationPhone; }
        }
        public static string ValidationSubName
        {
            get { return Resource.ValidationSubName; }
        }
        public static string ValidationEmail
        {
            get { return Resource.ValidationEmail; }
        }

        public static string ValidationTI
        {
            get { return Resource.ValidationTI; }
        }
        public static string ValidationAge
        {
            get { return Resource.ValidationAge; }
        }
        
        public static string MessageUserCreated
        {
            get { return Resource.MessageUserCreated; }
        }
        public static string MessagePasswordChanged
        {
            get { return Resource.MessagePasswordChanged; }
        }
        public static string MessageLogiIn
        {
            get { return Resource.MessageLogiIn; }
        }
        public static string MyReservation
        {
            get { return Resource.MyReservation; }
        }
        public static string Profile
        {
            get { return Resource.Profile; }
        }
        public static string Logout
        {
            get { return Resource.Logout; }
        }
    }

}
