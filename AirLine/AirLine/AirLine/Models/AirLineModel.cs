﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System.Collections.Generic;

namespace AirLine.Models
{
    public class AirLineModel
    {
        [PrimaryKey]
        public int AirLineModelId { get; set; }

        public string RUC { get; set; }

        public string Name { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.CascadeRead)]
        public List<AirPlane> AirPlanes{ get; set; }

        public override int GetHashCode()
        {
            return AirLineModelId;
        }
    }
}
