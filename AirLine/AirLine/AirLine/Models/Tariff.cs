﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System.Collections.Generic;

namespace AirLine.Models
{
    public class Tariff
    {
        [PrimaryKey]
        public int FlyId { get; set; }

        public string Class { get; set; }//CLase

        public double Price{ get; set; }

        public double Impost { get; set; } //Impuesto
         

        [OneToMany(CascadeOperations = CascadeOperation.CascadeRead)]
        public List<Fly> Fly { get; set; }


        public override int GetHashCode()
        {
            return FlyId;
        }
    }
}
