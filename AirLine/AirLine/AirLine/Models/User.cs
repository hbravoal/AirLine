﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace AirLine.Models
{
    public class User
    {
        [PrimaryKey]
        public int UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public string Picture { get; set; }
    


        public byte[] imageArray { get; internal set; }

        public string AccessToken { get; set; }

        public string TokenType { get; set; }

        public DateTime TokenExpires { get; set; }

        public bool IsRemembered { get; set; }

        public int UserTypeId { get; set; }
        [ManyToOne]
        public UserType UserType { get; set; }


        public int ReservationId { get; set; }
        [ManyToOne]
        public Reservation Reservation { get; set; }

        public string FullPicture
        {
            get
            {
                if (string.IsNullOrEmpty(Picture))
                {
                    return "upload_photo2.png";
                }

                if (UserTypeId != 2)
                {
                   Picture= string.Format("http://delcristo25-001-site1.ctempurl.com{0}", Picture.Substring(1));
                }

                return Picture;
            }
        }

        public override int GetHashCode()
        {
            return UserId;
        }

    }
}
