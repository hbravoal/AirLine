﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System.Collections.Generic;

namespace AirLine.Models
{
    public class AirPlane
    {
        [PrimaryKey]
        public int AirPlaneId { get; set; }

        public int AirLineModelId { get; set; }

        public string Producer { get; set; }

        public string Type { get; set; }

        public string Capacity { get; set; }

        [ManyToOne]
        public AirLineModel AirLineModel { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.CascadeRead)]
        public List<Fly> Flys{ get; set; }

        

        public override int GetHashCode()
        {
            return AirPlaneId;
        }
    }
}
