﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System.Collections.Generic;

namespace AirLine.Models
{
    public class Airport
    {
        [PrimaryKey]
        public int AirPortId { get; set; }

        public string Name { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.CascadeRead)]
        public List<Fly> Flys { get; set; }
        
        public override int GetHashCode()
        {
            return AirPortId;
        }
    }
}
