﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirLine.Models
{
    public class Bed
    {
        [PrimaryKey]
        public int BedId { get; set; }

        public string Letter { get; set; }

        public string File { get; set; }
        
        [OneToMany(CascadeOperations = CascadeOperation.CascadeRead)]
        public List<Fly>Flys{ get; set; }


    }
}
