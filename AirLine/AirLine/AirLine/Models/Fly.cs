﻿using AirLine.Services;
using GalaSoft.MvvmLight.Command;
using Newtonsoft.Json;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System.Windows.Input;
using System;
using AirLine.ViewModels;

namespace AirLine.Models
{
    public class Fly
    {

        #region Attributes
        private NavigationService navigationService;
        private DialogService dialogService;
        #endregion
        #region Constructor
        public Fly()
        {
            dialogService = new DialogService();
            navigationService = new NavigationService();
        } 
        #endregion
        #region Properties
        public int FlyId { get; set; }

        public int AirPlaneId { get; private set; }

        [ManyToOne]
        public AirPlane AirPlane { get; set; }

        public int BedId { get; private set; }
        [ManyToOne]
        public Bed Bed { get; set; }


        public int TariffId { get; private set; }
        [ManyToOne]
        public Tariff Tariff { get; set; }


        public int ReservationId { get; private set; }
        [ManyToOne]
        public Reservation Reservation { get; set; }

        public int AirportId { get; private set; }
        [ManyToOne]
        public Airport Airport { get; set; }

        public override int GetHashCode()
        {
            return FlyId;
        }
        #endregion
        #region Commands
        public ICommand SelectAirplaneCommand { get { return new RelayCommand(Details); } }
        #endregion
        #region Methods
        private async void Details()
        {
            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.Details = new DetailsViewModel(this);

            await navigationService.Navigate("DetailsPage");
        } 
        #endregion
    }
}
