﻿using GalaSoft.MvvmLight.Command;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace AirLine.Models
{
   public class Reservation
    {
        [PrimaryKey]
        public int ReservationId { get; set; }
        
        public double Price { get; set; }
        public DateTime Date{ get; set; }
        public string Observations { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.CascadeRead)]
        public List<Fly> Flys { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.CascadeRead)]
        public List<User> Users { get; set; }


        public override int GetHashCode()
        {
            return ReservationId;
        }

        
    }
}


