﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;


namespace AirLine.Models
{
    public class Pay
    {
        [PrimaryKey]
        public int PayId { get; set; }

        public int ReservationId { get; set; }
        [ManyToOne]
        public Reservation Reservations { get; set; }

        public DateTime Date { get; set; }


        

        public double   TotalPay{ get; set; }
       

        

        public override int GetHashCode()
        {
            return PayId;
        }
    }
}
