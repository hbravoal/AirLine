﻿
using AirLine.Classes;
using AirLine.Models;
using AirLine.Pages;
using AirLine.Services;
using AirLine.ViewModels;
using System;
using Xamarin.Forms;

namespace AirLine
{
    public partial class App : Application
    {
        #region Attributes
        private DataService dataService;
        #endregion

        #region Properties

        public static NavigationPage Navigator { get; internal set; }
        public static MasterPage Master { get; internal set; }
        #endregion

        public App()
        {
            InitializeComponent();
            dataService = new DataService();
            LoadParameters();
            var user = dataService.First<User>(false);
            if (user != null && user.IsRemembered && user.TokenExpires > DateTime.Now)
            {
               
                var mainViewModel = MainViewModel.GetInstance();

                mainViewModel.CurrentUser = user;
                HomeViewModel.GetInstance().LoadLocalData();
                HomeViewModel.GetInstance().LoadLocalDataOutConnection();
                mainViewModel.Home = new HomeViewModel();
                mainViewModel.Profile = new ProfileViewModel(user);

                MainPage = new MasterPage();
            }
            else
            {
                MainPage = new LoginPage();
            }
        }

        #region Methods
        public static Action HideLoginView
        {
            get
            {
                return new Action(() => App.Current.MainPage = new LoginFacebookPage());
            }
        }

        public static async void NavigateToProfile(FacebookResponse profile)
        {
            var apiService = new ApiService();
            var dialogService = new DialogService();
            var navigationService = new NavigationService();
            var dataService = new DataService();

            var parameters = dataService.First<Parameter>(false);
            var token = await apiService.LoginFacebook(
                parameters.URLBase, "/api", "/Users/LoginFacebook", profile);
            if (token == null)
            {
                App.Current.MainPage = new MasterPage();
                return;
            }

            var response = await apiService.GetUserByEmail(
                parameters.URLBase,
                "/api",
                "/Users/GetUserByEmail",
                token.TokenType,
                token.AccessToken,
                token.UserName);

            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage("Error", "Problem ocurred retrieving user information, try latter.");
                return;
            }

            var user = (User)response.Result;
            user.AccessToken = token.AccessToken;
            user.TokenType = token.TokenType;
            user.TokenExpires = token.Expires;
            user.IsRemembered = true;
            user.Password = profile.Id;
            user.UserTypeId = 2;
            dataService.DeleteAllAndInsert(user.UserType);            
            dataService.DeleteAllAndInsert(user);
            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.currentUser = user;

            navigationService.SetMainPage("MasterPage");
        }

        private void LoadParameters()
        {
            var urlBase = Application.Current.Resources["URLBase"].ToString();
            
            var parameter = dataService.First<Parameter>(false);
            if (parameter == null)
            {
                parameter = new Parameter
                {
                    URLBase = urlBase,
                    
                };

                dataService.Insert(parameter);
            }
            else
            {
                parameter.URLBase = urlBase;
                
                dataService.Update(parameter);
            }
        }
        #endregion
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
